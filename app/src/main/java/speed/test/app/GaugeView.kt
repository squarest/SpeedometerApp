package speed.test.app

import android.animation.TypeEvaluator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import kotlin.math.cos
import kotlin.math.min
import kotlin.math.sin

class GaugeView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var value = 0.0
    private val needleColor: Int
    private val backColor: Int
    private val ticksColor: Int
    private var valueColor: Int
    private val units: String
    private val maxValue: Int
    private val majorTicksCount: Int
    private val minorTicksCount: Int

    private val backgroundPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
    }
    private val needlePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        strokeWidth = NEEDLE_STROKE_WIDTH
        style = Paint.Style.STROKE
    }
    private val needleArcPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
    }
    private val ticksPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        strokeWidth = TICKS_STROKE_WIDTH
        style = Paint.Style.STROKE
    }
    private val ticksTextPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        textSize = LABELS_TEXT_SIZE.dpToPx.toFloat()
        textAlign = Paint.Align.CENTER
        isLinearText = true
    }
    private val valuePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        textSize = UNITS_TEXT_SIZE.dpToPx.toFloat()
        textAlign = Paint.Align.CENTER
        isLinearText = true
    }
    private val valueLinePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        strokeWidth = LINE_STROKE_WIDTH
    }
    private val baseLinePaintPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        strokeWidth = LINE_STROKE_WIDTH
    }

    init {
        context.obtainStyledAttributes(attrs, R.styleable.GaugeView, 0, 0).apply {
            needleColor = getColor(R.styleable.GaugeView_needleColor, ContextCompat.getColor(context, R.color.pink))
            backColor = getColor(R.styleable.GaugeView_backColor, ContextCompat.getColor(context, R.color.lightPurple))
            ticksColor = getColor(R.styleable.GaugeView_linesColor, ContextCompat.getColor(context, R.color.lightGray))
            valueColor = getColor(R.styleable.GaugeView_valueColor, ContextCompat.getColor(context, R.color.white))
            maxValue = getInteger(R.styleable.GaugeView_maxValue, DEFAULT_MAX_VALUE)
            majorTicksCount = getInteger(R.styleable.GaugeView_majorTicksCount, DEFAULT_MAJOR_TICKS_COUNT)
            minorTicksCount = getInteger(R.styleable.GaugeView_minorTicksCount, DEFAULT_MINOR_TICKS_COUNT)
            units = getString(R.styleable.GaugeView_units) ?: context.getString(R.string.units_km_h)
            recycle()
        }
        needlePaint.color = needleColor
        needleArcPaint.color = needleColor
        valueLinePaint.color = needleColor
        backgroundPaint.color = backColor
        ticksPaint.color = ticksColor
        ticksTextPaint.color = ticksColor
        baseLinePaintPaint.color = ticksColor
        valuePaint.color = valueColor
    }

    fun updateValue(newValue: Double) {
        val correctedValue = if (newValue > maxValue) {
            maxValue
        } else {
            newValue
        }.toDouble()
        val va: ValueAnimator = ValueAnimator.ofObject(
            TypeEvaluator<Double> { fraction, startValue, endValue -> (startValue + fraction * (endValue - startValue)) },
            value,
            correctedValue
        )
        va.duration = NEEDLE_ANIMATION_DURATION
        va.addUpdateListener { animation ->
            value = animation.animatedValue as Double
            invalidate()
        }
        va.start()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawBackground(canvas)
        drawValue(canvas)
        drawTicks(canvas)
        drawNeedle(canvas)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        var width: Int
        var height: Int

        width = if (widthMode == MeasureSpec.EXACTLY || widthMode == MeasureSpec.AT_MOST) {
            widthSize
        } else {
            -1
        }

        height = if (heightMode == MeasureSpec.EXACTLY || heightMode == MeasureSpec.AT_MOST) {
            heightSize
        } else {
            -1
        }
        if (height >= 0 && width >= 0) {
            width = min(height, width)
            height = width / 2
        } else if (width >= 0) {
            height = width / 2
        } else if (height >= 0) {
            width = height * 2
        } else {
            width = 0
            height = 0
        }

        //MUST CALL THIS
        setMeasuredDimension(width, height)
    }

    private fun drawNeedle(canvas: Canvas) {
        val oval: RectF = getOval(canvas, 1f)
        val radius: Float = oval.width() * 0.4f
        val smallOval: RectF = getOval(canvas, 0.1f)
        val angle = 10 + (value / maxValue * 160).toFloat()

        canvas.drawLine(
            (oval.centerX() + cos((180 - angle) / 180 * Math.PI) * smallOval.width() * 0.5f).toFloat(),
            (oval.centerY() - sin(angle / 180 * Math.PI) * smallOval.width() * 0.5f).toFloat(),
            (oval.centerX() + cos((180 - angle) / 180 * Math.PI) * radius).toFloat(),
            (oval.centerY() - sin(angle / 180 * Math.PI) * radius).toFloat(),
            needlePaint
        )
        canvas.drawArc(smallOval, 180f, 180f, true, needleArcPaint)
    }

    private fun drawTicks(canvas: Canvas) {
        val availableAngle = 160f
        val majorTickStep = maxValue / (majorTicksCount - 1)
        val majorTickStepAngle = (majorTickStep / maxValue.toFloat() * availableAngle)
        val minorTickStepAngle = majorTickStepAngle / (1 + minorTicksCount)
        val majorTicksLength = 30f
        val minorTicksLength = majorTicksLength / 2
        val oval: RectF = getOval(canvas, 1f)
        val radius: Float = oval.width() * 0.4f
        var currentAngle = 10f
        var curProgress = 0
        while (currentAngle <= 170) {
            canvas.drawLine(
                (oval.centerX() + cos((180 - currentAngle) / 180 * Math.PI) * (radius - majorTicksLength / 2)).toFloat(),
                (oval.centerY() - sin(currentAngle / 180 * Math.PI) * (radius - majorTicksLength / 2)).toFloat(),
                (oval.centerX() + cos((180 - currentAngle) / 180 * Math.PI) * (radius + majorTicksLength / 2)).toFloat(),
                (oval.centerY() - sin(currentAngle / 180 * Math.PI) * (radius + majorTicksLength / 2)).toFloat(),
                ticksPaint
            )
            for (i in 1..minorTicksCount) {
                val angle = currentAngle + i * minorTickStepAngle
                if (angle >= 170 + minorTickStepAngle / 2) {
                    break
                }
                canvas.drawLine(
                    (oval.centerX() + cos((180 - angle) / 180 * Math.PI) * radius).toFloat(),
                    (oval.centerY() - sin(angle / 180 * Math.PI) * radius).toFloat(),
                    (oval.centerX() + cos((180 - angle) / 180 * Math.PI) * (radius + minorTicksLength)).toFloat(),
                    (oval.centerY() - sin(angle / 180 * Math.PI) * (radius + minorTicksLength)).toFloat(),
                    ticksPaint
                )
            }
            canvas.save()
            canvas.rotate(180 + currentAngle, oval.centerX(), oval.centerY())
            val txtX: Float = oval.centerX() + radius + majorTicksLength / 2 + 8
            val txtY: Float = oval.centerY()
            canvas.rotate(+90f, txtX, txtY)
            canvas.drawText(curProgress.toString(), txtX, txtY, ticksTextPaint)
            canvas.restore()
            currentAngle += majorTickStepAngle
            curProgress += majorTickStep
        }
        val smallOval: RectF = getOval(canvas, 0.8f)
        valueLinePaint.color = needleColor
        canvas.drawArc(smallOval, 185f, 170f, false, baseLinePaintPaint)
        canvas.drawArc(smallOval, 185f, 10 + (value / maxValue * 150).toFloat(), false, valueLinePaint)
    }

    private fun drawBackground(canvas: Canvas) {
        val oval: RectF = getOval(canvas, 1f)
        canvas.drawArc(oval, 180f, 180f, true, backgroundPaint)
    }

    private fun drawValue(canvas: Canvas) {
        val oval: RectF = getOval(canvas, 1f)
        canvas.drawText("${value.toInt()} $units", canvas.width / 2f, canvas.height - oval.height() / 5, valuePaint)
    }

    private fun getOval(canvas: Canvas, factor: Float): RectF {
        val oval: RectF
        val canvasWidth = canvas.width - paddingLeft - paddingRight
        val canvasHeight = canvas.height - paddingTop - paddingBottom
        oval = if (canvasHeight * 2 >= canvasWidth) {
            RectF(0F, 0F, canvasWidth * factor, canvasWidth * factor)
        } else {
            RectF(0F, 0F, canvasHeight * 2 * factor, canvasHeight * 2 * factor)
        }
        oval.offset((canvasWidth - oval.width()) / 2 + paddingLeft, (canvasHeight * 2 - oval.height()) / 2 + paddingTop)
        return oval
    }

    companion object {
        const val DEFAULT_MAX_VALUE = 250
        const val DEFAULT_MAJOR_TICKS_COUNT = 6
        const val DEFAULT_MINOR_TICKS_COUNT = 4
        const val NEEDLE_ANIMATION_DURATION = 1000L
        const val LABELS_TEXT_SIZE = 16
        const val UNITS_TEXT_SIZE = 24
        const val NEEDLE_STROKE_WIDTH = 8f
        const val TICKS_STROKE_WIDTH = 3f
        const val LINE_STROKE_WIDTH = 5f
    }
}
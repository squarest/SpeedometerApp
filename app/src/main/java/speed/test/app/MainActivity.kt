package speed.test.app

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.view.MotionEvent
import android.view.WindowInsets
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import kotlin.math.abs
import kotlin.math.sin

class MainActivity : AppCompatActivity() {
    private lateinit var speedometer: GaugeView
    private lateinit var tahometer: GaugeView
    private var screenWidth = 0f
    private var touchStartX = 0f
    private var touchStopX = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        configureWindow()
        initViews()
        startValuesGeneration()
    }

    private fun configureWindow() {
        screenWidth = windowManager.defaultDisplay.width.toFloat()
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }
    }

    private fun initViews() {
        speedometer = findViewById(R.id.speedometerGaugeView)
        tahometer = findViewById<GaugeView?>(R.id.tahometerGaugeView).apply {
            translationX = screenWidth
        }
    }

    private fun startValuesGeneration() {
        val handlerThread = HandlerThread("generationThread")
        handlerThread.start()
        val handler = Handler(handlerThread.looper)
        var periodOffset = 0
        handler.post(object : Runnable {
            override fun run() {
                val speed = generatePeriodicValue(periodOffset, 100, 130)
                val taho = generatePeriodicValue(periodOffset, 1, 8)
                speedometer.updateValue(speed)
                tahometer.updateValue(taho)
                periodOffset++
                handler.postDelayed(this, 1000)
            }
        })
    }

    private fun generatePeriodicValue(periodOffset: Int, from: Int, to: Int) =
        abs(sin(periodOffset.toDouble())) * (to - from) + from

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event == null) return super.onTouchEvent(event)
        when (event.action and MotionEvent.ACTION_MASK) {
            MotionEvent.ACTION_POINTER_DOWN -> {
                touchStartX = event.getX(0)
            }
            MotionEvent.ACTION_POINTER_UP -> {
                if (touchStartX > touchStopX) {
                    //moved left
                    tahometer.animate().translationX(0f).start()
                } else {
                    //moved right
                    tahometer.animate().translationX(screenWidth).start()
                }
            }
            MotionEvent.ACTION_MOVE -> {
                touchStopX = event.getX(0)
                val diff = touchStartX - touchStopX
                if (diff > 0 && tahometer.translationX != 0f) {
                    //moving left
                    tahometer.translationX = screenWidth - diff
                } else if (diff < 0 && tahometer.translationX != screenWidth) {
                    //moving right
                    tahometer.translationX = -diff
                }
            }
        }
        return true
    }
}
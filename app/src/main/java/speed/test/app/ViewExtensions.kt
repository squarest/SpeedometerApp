package speed.test.app

import android.content.res.Resources
import android.util.TypedValue

val Int.dpToPx: Int
    get() {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            this.toFloat(),
            Resources.getSystem().displayMetrics
        ).toInt()
    }